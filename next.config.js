module.exports = {
    env: {
        REACT_APP_GENESIS_ENV: 'dev',
        API_URL: 'https://server.ascentfs.sg/',
        SINGPASS_CALL_BACK_URL: 'http://localhost:3001/callback',
        PUBLIC_URL: '/',
    },
  }