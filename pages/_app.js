import '../styles/theme.scss';
import React, { useState,useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Sidenav } from '../components';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const authPages = [
    '/error-illustration',
    '/error',
    '/password-reset-cover',
    '/password-reset-illustration',
    '/password-reset',
    '/sign-in-cover',
    '/sign-in-illustration',
    '/sign-in',
    '/sign-up-cover',
    '/sign-up-illustration',
    '/sign-up',
  ];
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/favicon/favicon.ico" type="image/x-icon" />
        <title>Dashkit React</title>
      </Head>
      {!authPages.includes(router.pathname) && <Sidenav />}
      <div className="parent-container-root">
      <Component {...pageProps} />
      </div>
    </>
  );
}

export default MyApp;
