import FeatherIcon from 'feather-icons-react';
import React, { useState } from 'react';
import { Button, Card, Col, Container, Form, Nav, Row } from 'react-bootstrap';
import { Dropzone, Flatpickr, Quill, Select } from '../../../components/vendor';

export default function Wizard() {
  const [activeStep, setActiveStep] = useState(1);

  const stepOne = (
    <>
      <Row className="justify-content-center">
        <Col xs={12} md={10} lg={8} xl={6} className="text-center">
          <h6 className="mb-4 text-uppercase text-muted">Step {activeStep} of 5</h6>
          <h1 className="mb-3">Let’s start with the basics.</h1>
          <p className="mb-5 text-muted">
            please enter fund joining code, you received from your assets manager
          </p>
        </Col>
      </Row>
      <div className="form-group">
        <Form.Label>Fund Code</Form.Label>
        <Form.Control type="text" />
      </div>

      <hr className="my-5" />
      <Nav className="row align-items-center">
        <Col xs="auto">
          <Button variant="white" size="lg">
            Cancel
          </Button>
        </Col>
        <Col className="text-center">
          <h6 className="text-uppercase text-muted mb-0">Step {activeStep} of 5</h6>
        </Col>
        <Col xs="auto">
          <Button size="lg" onClick={() => setActiveStep(activeStep + 1)}>
            Continue
          </Button>
        </Col>
      </Nav>
    </>
  );
  const stepTwo = (
    <>
      <Row className="justify-content-center">
        <Col xs={12} md={10} lg={8} xl={6} className="text-center">
          <h6 className="mb-4 text-uppercase text-muted">Step {activeStep} of 5</h6>
          <h1 className="mb-3">Next, verify fund details</h1>
          <p className="mb-5 text-muted">
            We need you to verify fund information before proceeding
          </p>
        </Col>
      </Row>

      <div class="card">
        <div class="card-body text-center">


          <a href="team-overview.html" class="card-avatar avatar avatar-lg mx-auto">
            <img src="/img/investor/team-logo-1.jpeg" alt="" class="avatar-img rounded" />
          </a>


          <h2 class="mb-3">
            <a href="team-overview.html">Fist Men Digital Fund</a>
          </h2>


          <p class="card-text text-muted">
            FMDF is one of highest RoI fund with 10 years positive outlook, investing in fin tech startups
          </p>

        </div>
        <div class="card-footer card-footer-boxed">
          <div class="row align-items-center">
            <div class="col">


              <small class="text-muted">
                <i class="fe fe-clock"></i> Dealing every month
              </small>

            </div>
            <div class="col-auto">

              <small>KYC status</small> <i class="fe fe-check-circle text-success"></i>

            </div>
          </div>
        </div>
      </div>
      <hr className="my-5" />
      <Nav className="row align-items-center">
        <Col xs="auto">
          <Button variant="white" size="lg" onClick={() => setActiveStep(activeStep - 1)}>
            Back
          </Button>
        </Col>
        <Col className="text-center">
          <h6 className="text-uppercase text-muted mb-0">Step {activeStep} of 5</h6>
        </Col>
        <Col xs="auto">
          <Button size="lg" onClick={(e) => setActiveStep(activeStep + 1)}>
            Continue
          </Button>
        </Col>
      </Nav>
    </>
  );

  const stepThree = (
    <>
      <Row className="justify-content-center">
        <Col xs={12} md={10} lg={8} xl={6} className="text-center">
          <h6 className="mb-4 text-uppercase text-muted">Step {activeStep} of 5</h6>
          <h1 className="mb-3">Identity & Finance</h1>
          <p className="mb-5 text-muted">
            Attach one of your identity and finance to request
          </p>
        </Col>
      </Row>

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">


            <label class="form-label">
              Select your identity you want to attach to subscription request
            </label>


            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div className="form-group" style={{width:'100%'}}>
               
                <Select
                  options={[

                    {
                      value: 'Ii001',
                      label: '"Mr. A (individual) (US)',
                    },
                    {
                      value: 'Ii002',
                      label: 'ABC Corp. (business) (singapore',
                    },
                    {
                      value: 'Ii003',
                      label: 'Mr A (individual) (singapore)',
                    },
                    {
                      value: 'Ii004',
                      label: 'DEF Corp. (business) (US)',
                    },
                  ]}
                  placeholder={null}
                />
              </div>
              <div style={{ marginLeft: '10px' }}>
                <a href="{{ route('user.profile.identity.list') }}" class="btn btn-info" data-bs-toggle="tooltip" data-bs-placement="top" title="Add Identity"><span class="fe fe-plus"></span></a>
              </div>
            </div>


          </div>
          <div class="form-group">


            <label class="form-label">
              Select your Finance you want to attach to subscription request
            </label>


            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div className="form-group" style={{width:'100%'}}>
                
                <Select
                  options={[

                    {
                      value: 'Ii001',
                      label: '"Mr. A (individual) (US)',
                    },
                    {
                      value: 'Ii002',
                      label: 'ABC Corp. (business) (singapore',
                    },
                    {
                      value: 'Ii003',
                      label: 'Mr A (individual) (singapore)',
                    },
                    {
                      value: 'Ii004',
                      label: 'DEF Corp. (business) (US)',
                    },
                  ]}
                  placeholder={null}
                />
              </div>
              
              <div style={{ marginLeft: '10px' }}>
                <a href="{{ route('user.profile.identity.list') }}" class="btn btn-info" data-bs-toggle="tooltip" data-bs-placement="top" title="Add Finance"><span class="fe fe-plus"></span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card bg-light border" style={{ height: '100%' }}>
            <div class="card-body" style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>


              <h4 class="mb-2">
                <i class="fe fe-alert-triangle"></i> Warning
              </h4>


              <p class="small text-muted mb-0">
                Once an account is made, you cannot change identity attache to it and it type
              </p>

            </div>
          </div>
        </div>
      </div>

      <hr className="my-5" />
      <Nav className="row align-items-center">
        <Col xs="auto">
          <Button variant="white" size="lg" onClick={() => setActiveStep(activeStep - 1)}>
            Back
          </Button>
        </Col>
        <Col className="text-center">
          <h6 className="text-uppercase text-muted mb-0">Step {activeStep} of 5</h6>
        </Col>
        <Col xs="auto">
          <Button size="lg" onClick={(e) => setActiveStep(activeStep + 1)}>
            Continue
          </Button>
        </Col>
      </Nav>
    </>
  );
  const stepFour = (
    <>
      <Row className="justify-content-center">
        <Col xs={12} md={10} lg={8} xl={6} className="text-center">
          <h6 className="mb-4 text-uppercase text-muted">Step {activeStep} of 5</h6>
          <h1 className="mb-3">Which account you would like to setup.</h1>
          <p className="mb-5 text-muted">
            Whether id you want a standalone account or a joint account
          </p>
        </Col>
      </Row>

      <div class="row">
        <div class="col-12 col-md-6">

          <div class="form-group">

            <label class="form-label mb-1">
              Joint Account
            </label>
            <small class="form-text text-muted">
              If you are want to setup a joint account with others.
            </small>

            <div class="form-check form-switch">
              <input class="form-check-input" id="switchOne" type="checkbox" />
              <label class="form-check-label" for="switchOne">Enable</label>
            </div>

          </div>

        </div>
        <div class="col-12 col-md-6">

          <div class="card bg-light border">
            <div class="card-body">

              <h4 class="mb-2">
                <i class="fe fe-alert-triangle"></i> Warning
              </h4>

              <p class="small text-muted mb-0">
                Once an account is made, you cannot change identity attache to it and it type
              </p>

            </div>
          </div>

        </div>
      </div>
      <hr class="my-5" />

      <div class="form-group">

        <label class="form-label">
          Add partner emails to send them an invite
        </label>
        <input type="email" class="form-control" placeholder="Partner Email" />
      </div>

      <hr className="my-5" />
      <Nav className="row align-items-center">
        <Col xs="auto">
          <Button variant="white" size="lg" onClick={() => setActiveStep(activeStep - 1)}>
            Back
          </Button>
        </Col>
        <Col className="text-center">
          <h6 className="text-uppercase text-muted mb-0">Step {activeStep} of 5</h6>
        </Col>
        <Col xs="auto">
          <Button size="lg" onClick={(e) => setActiveStep(activeStep + 1)}>
            Continue
          </Button>
        </Col>
      </Nav>
    </>
  );
  const stepFive = (
    <>
      <Row className="justify-content-center">
        <Col xs={12} md={10} lg={8} xl={6} className="text-center">
          <h6 className="mb-4 text-uppercase text-muted">Step {activeStep} of 5</h6>
          <h1 className="mb-3">Summary</h1>
          <p className="mb-5 text-muted">
            Review your Information
          </p>
        </Col>
      </Row>

      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body text-center">

              <a href="team-overview.html" class="card-avatar avatar avatar-lg mx-auto">
                <img src="/img/investor/team-logo-1.jpeg" alt="" class="avatar-img rounded" />
              </a>

              <h2 class="mb-3">
                <a href="team-overview.html">Fist Men Digital Fund</a>
              </h2>

              <p class="card-text text-muted">
                FMDF is one of highest RoI fund with 10 years positive outlook, investing in fintect sartups
              </p>

            </div>
            <div class="card-footer card-footer-boxed">
              <div class="row align-items-center">
                <div class="col">

                  <small class="text-muted">
                    <i class="fe fe-clock"></i> Dealing every month
                  </small>

                </div>
                <div class="col-auto">

                  <small>KYC status</small> <i class="fe fe-check-circle text-success"></i>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body text-center">
              <h2>Identity/Finance</h2>
              <div class="summary-identity-finance">
                <p>Identity: <span>Mr. A (individual) (US)</span></p>
                <p>Finance: <span>Mr. A (Bank) (US)</span></p>

              </div>

            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body text-center">
              <h2>Joint Account</h2>
              <div class="summary-identity-finance">
                <ul>
                  <li>calvin@ascentfs.sg</li>
                  <li>zainabbas@ascentfs.sg</li>
                </ul>
              </div>


            </div>
          </div>
        </div>
        <div class="col-sm-6">

        </div>
      </div>
      <hr class="my-5" />
      <Nav className="row align-items-center">
        <Col xs="auto">
          <Button variant="white" size="lg" onClick={() => setActiveStep(activeStep - 1)}>
            Back
          </Button>
        </Col>
        <Col className="text-center">
          <h6 className="text-uppercase text-muted mb-0">Step {activeStep} of 5</h6>
        </Col>
        <Col xs="auto">
          <Button size="lg">
            Submit Request
          </Button>
        </Col>
      </Nav>
    </>
  );

  return (
    <div className="main-content">
      <Container fluid="lg">
        <Row className="justify-content-center">
          <Col xs={12} lg={10} xl={8} className="py-6">
            {activeStep === 1 && stepOne}
            {activeStep === 2 && stepTwo}
            {activeStep === 3 && stepThree}
            {activeStep === 4 && stepFour}
            {activeStep === 5 && stepFive}
          </Col>
        </Row>
      </Container>
    </div>
  );
}
