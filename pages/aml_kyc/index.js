import React, { useMemo } from 'react';
import {
    Col,
    Row,
    Container,
} from 'react-bootstrap';
import {
    DynamicHeader
} from '../../widgets';
import OrganizationAmlKyc from './organization/OrganizationAmlKyc';
export default function CRMContactsTable({ ...props }) {

    function headerButtonCallBack() {

    }
    return (
        <>
            <div className="main-content">
                <DynamicHeader style={{ marginBottom: '0rem' }} title="AML/KYC" titlesmall="OverView" buttontext={""} buttoncallback={headerButtonCallBack} />
                <Container fluid>
                    <Row className="justify-content-center">
                        <Col xs={12}>
                            <OrganizationAmlKyc />
                        </Col>
                    </Row>
                </Container>
            </div>


        </>
    );
}
