import { Col, Container, Row } from 'react-bootstrap';
import {
    AnalyticsAverageTime,
    AnalyticsConversions,
    AnalyticsExit,
    AnalyticsGoals,
    AnalyticsHeader,
    AnalyticsProjects,
    AnalyticsRecentActivity,
    AnalyticsSales,
    AnalyticsScratchpadChecklist,
    AnalyticsTotalHours,
    AnalyticsTrafficChannels,
    AnalyticsValue,
    DynamicHeader
} from '../../../widgets';

export default function Index() {


    function headerButtonCallBack() {
        console.log('headerButtonCallBack called')
    }
    return (
        <div className="main-content">
            <DynamicHeader title="Dashboard" titlesmall="OverView" buttontext="Create Report" buttoncallback={headerButtonCallBack} />
            <Container fluid>
                <Row>
                    <Col xs={12} md={6} xl>
                        <AnalyticsValue />
                    </Col>
                    <Col xs={12} md={6} xl>
                        <AnalyticsTotalHours />
                    </Col>
                    <Col xs={12} md={6} xl>
                        <AnalyticsExit />
                    </Col>
                    <Col xs={12} md={6} xl>
                        <AnalyticsAverageTime />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} xl={8}>
                        <AnalyticsConversions />
                    </Col>
                    <Col xs={12} xl={4}>
                        <AnalyticsTrafficChannels />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} xl={4}>
                        <AnalyticsProjects />
                    </Col>
                    <Col xs={12} xl={8}>
                        <AnalyticsSales />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <AnalyticsGoals />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} xl={5}>
                        <AnalyticsRecentActivity className="card-fill" />
                    </Col>
                    <Col xs={12} xl={7}>
                        <AnalyticsScratchpadChecklist />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
