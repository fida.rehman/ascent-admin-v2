import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import {
  Alerts,
  Autosizes,
  Avatars,
  Badges,
  Breadcrumbs,
  ButtonGroups,
  Buttons,
  Cards,
  Charts,
  Checklists,
  Dropdowns,
  Forms,
  Icons,
  Kanbans,
  Lists,
  Maps,
  Modals,
  Navbars,
  Navs,
  PageHeaders,
  Paginations,
  Popovers,
  Progresses,
  Spinners,
  Tables,
  Toasts,
  Tooltips,
  Typography,
  Utilities,
} from '../docs';

export default function Components() {
  return (
    <>
      <div className="main-content pb-6">
        <Container fluid>
          <Row className="justify-content-center">
            <Col xs={12} lg={10} xl={8}>
              <Alerts />
              <Autosizes />
              <Avatars />
              <Badges />
              <Breadcrumbs />
              <ButtonGroups />
              <Buttons />
              <Cards />
              <Charts />
              <Checklists />
              <Dropdowns />
              <Forms />
              <Icons />
              <Kanbans />
              <Lists />
              <Maps />
              <Modals />
              <Navbars />
              <Navs />
              <PageHeaders />
              <Paginations />
              <Popovers />
              <Progresses />
              <Spinners />
              <Tables />
              <Toasts />
              <Tooltips />
              <Typography />
              <Utilities />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
