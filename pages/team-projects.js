import React from 'react';
import { projects } from '../data';
import { Projects, TeamHeader } from '../widgets';

export default function TeamProjects() {
  return (
    <div className="main-content">
      <TeamHeader />
      <Projects projects={projects} />
    </div>
  );
}
