import Link from 'next/link';
import React, { useMemo } from 'react';
import {
    Row,
} from 'react-bootstrap';

export default function InvestorSubscriptionList({ ...props }) {


    function headerButtonCallBack() {

    }
    function pageNumberChangedCallback() {

    }
    return (
        <>
            <div className="main-content">
                <Row className="justify-content-center">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-header-title">
                                <img src="/img/investor/team-logo-1.jpeg" style={{maxHeight:'30px', textAlign:'left'}} alt="" class="rounded" />
                                Digital Reds Fund
                            </h4>
                            {/* <a href="investor/investor/investorSubscriptionDetail.js" class="btn btn-sm btn-white me-2">Investment Details</a> */}
                            <a href="#" class="btn btn-sm btn-white me-2">Dashboard</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col ms-n2">
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-clock text-success"></i> Dealing every month
                                                        </small>
                                                    </div>

                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-warning"></i> Fund's KYC
                                                        </small>
                                                    </div>
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-success"></i> Digital Fund
                                                        </small>
                                                    </div>
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-success"></i> Singapore
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col-auto">

                                                    <a href="#!" class="avatar avatar-lg">
                                                        <img src="/img/investor/default-avatar.png" alt="..." class="avatar-img rounded-circle" />
                                                    </a>

                                                </div>
                                                <div class="col ms-n2">

                                                    <h4 class="mb-1">
                                                        <a href="{{ route('user.profile.identity.individual.summary') }}">John Doe</a>
                                                    </h4>

                                                    <p class="small text-muted mb-1">
                                                        Citizenship : US <i class="fe fe-check-circle text-success"></i><br />
                                                        Bank Account : <a href="{{ route('user.profile.identity.bank.summary') }}">BoA (US3258BOA23784638)</a> <i class="fe fe-check-circle text-success"></i>
                                                    </p>

                                                    <p class="small mb-0">
                                                        <span class="text-success">●</span> Subscription Type : Standalone
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-header-title">
                                <img src="/img/investor/team-logo-1.jpeg" style={{ maxHeight: '30px', textAlign: 'left' }} alt="" class="rounded" />
                                Digital Blues Fund
                            </h4>
                            <a href="#" class="btn btn-sm btn-danger me-2">Sign Agreement</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col ms-n2">
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-clock text-success"></i> Dealing every month
                                                        </small>
                                                    </div>

                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-warning"></i> Fund's KYC
                                                        </small>
                                                    </div>
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-success"></i> Digital Fund
                                                        </small>
                                                    </div>
                                                    <div class="row align-items-center">

                                                        <small class="text-muted">
                                                            <i class="fe fe-check-circle text-success"></i> Singapore
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col-auto">

                                                    <a href="#!" class="avatar avatar-lg">
                                                        <img src="/img/investor/default-avatar.png" alt="..." class="avatar-img rounded-circle" />
                                                    </a>

                                                </div>
                                                <div class="col ms-n2">

                                                    <h4 class="mb-1">
                                                        <a href="{{ route('user.profile.identity.individual.summary') }}">John Doe</a>
                                                    </h4>

                                                    <p class="small text-muted mb-1">
                                                        Citizenship : US <i class="fe fe-check-circle text-success"></i><br />
                                                        Bank Account : <a href="{{ route('user.profile.identity.bank.summary') }}">BoA (US3258BOA23784638)</a> <i class="fe fe-check-circle text-success"></i>
                                                    </p>

                                                    <p class="small mb-0">
                                                        <span class="text-success">●</span> Subscription Type : Standalone
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Row>

            </div>


        </>
    );
}
