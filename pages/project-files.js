import { Col, Container, Row } from 'react-bootstrap';
import { files } from '../data';
import { Files, ProjectHeader } from '../widgets';

export default function ProjectFiles() {
  return (
    <div className="main-content">
      <ProjectHeader />
      <Container fluid>
        <Row>
          <Col xs={12}>
            <Files files={files} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
