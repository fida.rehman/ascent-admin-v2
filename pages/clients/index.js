import React, { useMemo } from 'react';
import {
    Col,
    Row,
    Container,
} from 'react-bootstrap';
import {
    DynamicHeader
} from '../../widgets';
import OrganizationClients from './organization/OrganizationClients';
export default function CRMContactsTable({ ...props }) {

    function headerButtonCallBack() {

    }
    return (
        <>
            <div className="main-content">
                <DynamicHeader style={{ marginBottom: '0rem' }} title="Clients" titlesmall="OverView" buttontext={""} buttoncallback={headerButtonCallBack} />
                <Container fluid>
                    <Row className="justify-content-center">
                        <Col xs={12}>
                            <OrganizationClients />
                        </Col>
                    </Row>
                </Container>
            </div>


        </>
    );
}
