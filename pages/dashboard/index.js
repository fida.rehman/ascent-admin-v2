import React, { useMemo } from 'react';
import {
    Col,
    Row,
    Container,
} from 'react-bootstrap';
import {
    DynamicHeader
} from '../../widgets';
import OrganizationDashboard from './organization/OrganizationDashboard';
export default function CRMContactsTable({ ...props }) {

    function headerButtonCallBack() {

    }
    return (
        <>
            <div className="main-content">
                <DynamicHeader style={{ marginBottom: '0rem' }} title="Dashboard" titlesmall="OverView" buttontext={"New Fund"} buttoncallback={headerButtonCallBack} />
                <Container fluid>
                    <Row className="justify-content-center">
                        <Col xs={12}>
                            <OrganizationDashboard />
                        </Col>
                    </Row>
                </Container>
            </div>


        </>
    );
}
