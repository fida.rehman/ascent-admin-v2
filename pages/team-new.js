import FeatherIcon from 'feather-icons-react';
import { Button, Card, Col, Container, Form, Row } from 'react-bootstrap';
import { Header } from '../components';
import { Dropzone, Quill, Select } from '../components/vendor';

export default function ProfilePosts() {
  return (
    <div className="main-content">
      <Container fluid>
        <Row className="justify-content-center">
          <Col xs={12} lg={10} xl={8}>
            <Header className="mt-md-5">
              <Header.Body>
                <Row className="align-items-center">
                  <Col>
                    <Header.Pretitle>New team</Header.Pretitle>
                    <Header.Title>Create a new team</Header.Title>
                  </Col>
                </Row>
              </Header.Body>
            </Header>
            <form className="mb-4">
              <div className="form-group">
                <Form.Label>Team name</Form.Label>
                <Form.Control type="text" />
              </div>
              <div className="form-group">
                <Form.Label className="mb-1">Team description</Form.Label>
                <Form.Text className="text-muted">
                  This is how others will learn about the project, so make it good!
                </Form.Text>
                <Quill />
              </div>
              <div className="form-group">
                <Form.Label>Add team members</Form.Label>
                <Select
                  options={[
                    {
                      imgSrc: '/img/avatars/profiles/avatar-1.jpg',
                      label: 'Dianna Smiley',
                      value: 'Dianna Smiley',
                    },
                    {
                      imgSrc: '/img/avatars/profiles/avatar-2.jpg',
                      value: 'Ab Hadley',
                      label: 'Ab Hadley',
                    },
                    {
                      imgSrc: '/img/avatars/profiles/avatar-3.jpg',
                      value: 'Adolfo Hess',
                      label: 'Adolfo Hess',
                    },
                    {
                      imgSrc: '/img/avatars/profiles/avatar-4.jpg',
                      value: 'Daniela Dewitt',
                      label: 'Daniela Dewitt',
                    },
                  ]}
                  placeholder={null}
                  isMulti
                />
              </div>
              <hr className="mt-4 mb-5" />
              <div className="form-group">
                <Form.Label className="mb-1">Team cover</Form.Label>
                <Form.Text className="text-muted">Please use an image no larger than 1200px * 600px.</Form.Text>
                <Dropzone accept="image/*" multiple={false} onDrop={(acceptedFiles) => console.log(acceptedFiles)} />
              </div>
              <hr className="mt-5 mb-5" />
              <Row>
                <Col xs={12} md={6}>
                  <div className="form-group">
                    <Form.Label className="mb-1">Private team</Form.Label>
                    <Form.Text className="text-muted">
                      If you are available for hire outside of the current situation, you can encourage others to hire
                      you.
                    </Form.Text>
                    <Form.Switch />
                  </div>
                </Col>
                <Col xs={12} md={6}>
                  <Card className="bg-light border">
                    <Card.Body>
                      <h4 className="mb-2">
                        <FeatherIcon icon="alert-triangle" size="1em" /> Warning
                      </h4>
                      <p className="small text-muted mb-0">
                        Once a team is made private, you cannot revert it to a public team.
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
              <hr className="mt-5 mb-5" />
              <Button className="w-100">Create team</Button>
              <Button variant="link" className="w-100 text-muted">
                Cancel this team
              </Button>
            </form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
