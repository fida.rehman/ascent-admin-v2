import FeatherIcon from 'feather-icons-react';
import Link from 'next/link';
import React, { useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import {
  Badge,
  Button,
  ButtonGroup,
  Card,
  CloseButton,
  Col,
  Container,
  Dropdown,
  Form,
  ListGroup,
  OverlayTrigger,
  Row,
  Tooltip,
} from 'react-bootstrap';
import { Avatar, Header, Kanban as KanbanAlias } from '../components';
import { Flatpickr } from '../components/vendor';
import { tasks } from '../data';
import { formatLocaleDateString, getStatusColor } from '../helpers';
import { ModalKanbanTask, ModalKanbanTaskEmpty } from '../modals';

export default function Kanban() {
  const [data, setData] = useState(tasks);
  const [activeTab, setActiveTab] = useState(0);
  const [addCardOpen, setAddCardOpen] = useState(false);
  const [addListOpen, setAddListOpen] = useState(false);
  const [modalTaskVisible, setModalTaskVisible] = useState(false);
  const [modalTaskEmptyVisible, setModalTaskEmptyVisible] = useState(false);

  function onDragEnd(result) {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    const start = data.columns[source.droppableId];
    const finish = data.columns[destination.droppableId];

    if (start === finish) {
      const newItemIds = Array.from(start.itemIds);

      newItemIds.splice(source.index, 1);
      newItemIds.splice(destination.index, 0, draggableId);

      const newColumn = {
        ...start,
        itemIds: newItemIds,
      };

      const newData = {
        ...data,
        columns: { ...data.columns, [newColumn.id]: newColumn },
      };

      setData(newData);

      return;
    }

    const startItemIds = Array.from(start.itemIds);
    startItemIds.splice(source.index, 1);

    const newStart = {
      ...start,
      itemIds: startItemIds,
    };

    const finishItemIds = Array.from(finish.itemIds);
    finishItemIds.splice(destination.index, 0, draggableId);

    const newFinish = {
      ...finish,
      itemIds: finishItemIds,
    };

    const newData = {
      ...data,
      columns: {
        ...data.columns,
        [newStart.id]: newStart,
        [newFinish.id]: newFinish,
      },
    };

    setData(newData);
  }

  const dropdown = (
    <Dropdown align="end">
      <Dropdown.Toggle as="span" className="dropdown-ellipses" role="button">
        <FeatherIcon icon="more-vertical" size="17" />
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Item href="#!">Action</Dropdown.Item>
        <Dropdown.Item href="#!">Another action</Dropdown.Item>
        <Dropdown.Item href="#!">Something else here</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );

  const itemsOne = data.columnOrder.map((columnId, index) => {
    const column = data.columns[columnId];

    return (
      <Col xs={12} key={index}>
        <Card>
          <Card.Header className="card-header-flush">
            <h4 className="card-header-title">{column.title}</h4>
            {dropdown}
          </Card.Header>
          <Card.Body>
            <Droppable droppableId={columnId}>
              {(provided) => (
                <KanbanAlias.Category ref={provided.innerRef} {...provided.droppableProps}>
                  {column.itemIds.map((itemId, index) => {
                    const item = data.items[itemId];

                    return (
                      <Draggable draggableId={item.id} key={item.id} index={index}>
                        {(provided, snapshot) => (
                          <KanbanAlias.Item
                            className="mb-3"
                            dragging={snapshot.isDragging}
                            dropped={snapshot.isDropAnimating}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Card
                              className="card-sm mb-0"
                              onClick={() =>
                                index % 2 === 0 ? setModalTaskEmptyVisible(true) : setModalTaskVisible(true)
                              }
                            >
                              {item.imgSrc && <Card.Img variant="top" src={item.imgSrc} alt={item.content} />}
                              <Card.Body>
                                {item.status && (
                                  <p className="mb-3">
                                    <Badge bg={`${getStatusColor(item.status)}-soft`}>{item.status}</Badge>
                                  </p>
                                )}
                                <p className="mb-0">{item.content}</p>
                                {(item.comments || item.date || item.user) && <p />}
                                <Row className="align-items-center">
                                  {(item.comments || item.date) && (
                                    <Col>
                                      <Card.Text className="small text-muted">
                                        {item.comments && (
                                          <span className="me-3">
                                            <FeatherIcon icon="message-circle" size="1em" /> {item.comments}
                                          </span>
                                        )}
                                        {item.date && (
                                          <span className="me-3">
                                            <FeatherIcon icon="clock" size="1em" />{' '}
                                            {formatLocaleDateString(item.date, {
                                              month: 'short',
                                              day: 'numeric',
                                            })}
                                          </span>
                                        )}
                                      </Card.Text>
                                    </Col>
                                  )}
                                  {item.users && (
                                    <Col xs="auto">
                                      <Avatar.Group>
                                        {item.users.map((user, key) => (
                                          <Avatar size="xs" key={key}>
                                            <OverlayTrigger overlay={<Tooltip>{user.title}</Tooltip>}>
                                              <Avatar.Image
                                                src={user.imgSrc}
                                                alt={user.title}
                                                className="rounded-circle"
                                              />
                                            </OverlayTrigger>
                                          </Avatar>
                                        ))}
                                      </Avatar.Group>
                                    </Col>
                                  )}
                                </Row>
                              </Card.Body>
                            </Card>
                          </KanbanAlias.Item>
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder}
                </KanbanAlias.Category>
              )}
            </Droppable>
            {addCardOpen === columnId ? (
              <Card className="card-sm mb-0">
                <Card.Body>
                  <form>
                    <div className="form-group">
                      <Form.Control
                        as="textarea"
                        className="form-control-auto form-control-flush"
                        placeholder="Draft your card"
                      />
                    </div>
                    <Row className="align-items-center">
                      <Col className="d-flex align-items-center">
                        <span className="text-muted small me-2">
                          <FeatherIcon icon="clock" size="1em" />
                        </span>
                        <Form.Control
                          as={Flatpickr}
                          className="form-control-auto form-control-flush"
                          placeholder="No due date"
                          size="sm"
                        />
                      </Col>
                      <Col xs="auto">
                        <Button variant="white" size="sm" onClick={() => setAddCardOpen(false)}>
                          Cancel
                        </Button>
                        <Button size="sm" className="ms-1">
                          Add
                        </Button>
                      </Col>
                    </Row>
                  </form>
                </Card.Body>
              </Card>
            ) : (
              <Card className="card-sm card-flush mb-0">
                <Card.Body>
                  <div className="text-center">
                    <a href="#!" onClick={() => setAddCardOpen(columnId)}>
                      Add Card
                    </a>
                  </div>
                </Card.Body>
              </Card>
            )}
          </Card.Body>
        </Card>
      </Col>
    );
  });

  const itemsTwo = data.columnOrder.map((columnId, index) => {
    const column = data.columns[columnId];

    return (
      <ListGroup.Item key={index}>
        <Row className="align-items-center mb-4">
          <Col>
            <h2 className="mb-0">{column.title}</h2>
          </Col>
          <Col xs="auto">{dropdown}</Col>
        </Row>
        <Droppable droppableId={columnId}>
          {(provided) => (
            <KanbanAlias.Category ref={provided.innerRef} {...provided.droppableProps}>
              {column.itemIds.map((itemId, index) => {
                const item = data.items[itemId];

                return (
                  <Draggable draggableId={item.id} key={item.id} index={index}>
                    {(provided, snapshot) => (
                      <KanbanAlias.Item
                        className="mb-3"
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <Card
                          className="card-sm mb-0"
                          onClick={() => (index % 2 === 0 ? setModalTaskEmptyVisible(true) : setModalTaskVisible(true))}
                        >
                          <Card.Body>
                            <Row className="align-items-center">
                              <Col xs={12} sm>
                                {item.status && (
                                  <Badge bg={`${getStatusColor(item.status)}-soft`} className="me-2">
                                    {item.status}
                                  </Badge>
                                )}
                                {item.content}
                              </Col>
                              {(item.comments || item.date) && (
                                <Col sm="auto">
                                  <Card.Text className="small text-muted">
                                    {item.comments && (
                                      <span className="me-3">
                                        <FeatherIcon icon="message-circle" size="1em" /> {item.comments}
                                      </span>
                                    )}
                                    {item.date && (
                                      <span className="me-3">
                                        <FeatherIcon icon="clock" size="1em" />{' '}
                                        {formatLocaleDateString(item.date, {
                                          month: 'short',
                                          day: 'numeric',
                                        })}
                                      </span>
                                    )}
                                  </Card.Text>
                                </Col>
                              )}
                              {item.users && (
                                <Col xs={12} sm="auto">
                                  <Avatar.Group>
                                    {item.users.map((user, key) => (
                                      <Link href="/profile-posts" key={key} passHref>
                                        <Avatar as="a" size="xs">
                                          <OverlayTrigger layout={<Tooltip>{user.title}</Tooltip>}>
                                            <Avatar.Image
                                              src={user.imgSrc}
                                              alt={user.title}
                                              className="rounded-circle"
                                            />
                                          </OverlayTrigger>
                                        </Avatar>
                                      </Link>
                                    ))}
                                  </Avatar.Group>
                                </Col>
                              )}
                            </Row>
                          </Card.Body>
                        </Card>
                      </KanbanAlias.Item>
                    )}
                  </Draggable>
                );
              })}
              {provided.placeholder}
            </KanbanAlias.Category>
          )}
        </Droppable>
        {addCardOpen === columnId ? (
          <Card className="card-sm mb-0">
            <Card.Body>
              <form>
                <Row className="align-items-center">
                  <Col xs={12} md>
                    <Form.Control className="form-control-auto form-control-flush" placeholder="Draft your card" />
                  </Col>
                  <Col xs={6} md="auto" className="d-flex align-items-center">
                    <span className="text-muted small me-2">
                      <FeatherIcon icon="clock" size="1em" />
                    </span>
                    <Form.Control
                      as={Flatpickr}
                      className="form-control-auto form-control-flush"
                      placeholder="No due date"
                      size="sm"
                    />
                  </Col>
                  <Col xs={6} md="auto">
                    <Button variant="white" size="sm" onClick={() => setAddCardOpen(false)}>
                      Cancel
                    </Button>
                    <Button size="sm" className="ms-1">
                      Add
                    </Button>
                  </Col>
                </Row>
              </form>
            </Card.Body>
          </Card>
        ) : (
          <Card className="card-sm card-flush mb-0">
            <Card.Body>
              <a href="#!" onClick={() => setAddCardOpen(columnId)}>
                + Add Card
              </a>
            </Card.Body>
          </Card>
        )}
      </ListGroup.Item>
    );
  });

  return (
    <>
      <div className="main-content">
        <Header>
          <Container fluid>
            <Header.Body>
              <Row className="align-items-center">
                <Col>
                  <Header.Pretitle>Kanban boards</Header.Pretitle>
                  <Header.Title>Landkit Update</Header.Title>
                </Col>
                <Col xs="auto">
                  <Button variant="white" className="me-2">
                    Edit
                  </Button>
                  <ButtonGroup className="nav d-inline-flex">
                    <Button variant="white" active={activeTab === 0} onClick={() => setActiveTab(0)}>
                      <FeatherIcon icon="grid" size="1em" />
                    </Button>
                    <Button variant="white" active={activeTab === 1} onClick={() => setActiveTab(1)}>
                      <FeatherIcon icon="list" size="1em" />
                    </Button>
                  </ButtonGroup>
                </Col>
              </Row>
            </Header.Body>
          </Container>
        </Header>
        {activeTab === 0 && (
          <div className="container-fluid kanban-container">
            <Row>
              <DragDropContext onDragEnd={onDragEnd}>{itemsOne}</DragDropContext>
              <Col xs={12}>
                <Card className="mb-0">
                  <Card.Body>
                    {addListOpen ? (
                      <form>
                        <div className="d-flex align-items-center">
                          <Form.Control
                            className="h4"
                            className="form-control-auto form-control-flush"
                            placeholder="Name your list"
                            type="text"
                          />
                          <CloseButton className="ms-3" onClick={() => setAddListOpen(false)} size="sm" type="reset" />
                        </div>
                      </form>
                    ) : (
                      <div className="text-center">
                        <a href="#!" onClick={() => setAddListOpen(true)}>
                          Add list
                        </a>
                      </div>
                    )}
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </div>
        )}
        {activeTab === 1 && (
          <Container fluid>
            <Row>
              <Col xs={12}>
                <ListGroup className="list-group-flush list-group-lg">
                  <DragDropContext onDragEnd={onDragEnd}>{itemsTwo}</DragDropContext>
                  <ListGroup.Item>
                    {addListOpen ? (
                      <form>
                        <Row className="align-items-center">
                          <Col xs={12} md>
                            <Form.Control
                              className="h2 form-control-auto form-control-flush"
                              placeholder="Name your list"
                            />
                          </Col>
                          <Col xs="auto">
                            <Button variant="white" size="sm" type="reset" onClick={() => setAddListOpen(false)}>
                              Cancel
                            </Button>
                            <Button size="sm" className="ms-1">
                              Add
                            </Button>
                          </Col>
                        </Row>
                      </form>
                    ) : (
                      <a className="h2" href="#!" onClick={() => setAddListOpen(true)}>
                        + Add List
                      </a>
                    )}
                  </ListGroup.Item>
                </ListGroup>
              </Col>
            </Row>
          </Container>
        )}
      </div>
      <ModalKanbanTask visible={modalTaskVisible} onDismiss={() => setModalTaskVisible(false)} />
      <ModalKanbanTaskEmpty visible={modalTaskEmptyVisible} onDismiss={() => setModalTaskEmptyVisible(false)} />
    </>
  );
}
