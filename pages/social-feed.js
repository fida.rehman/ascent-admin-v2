import { Col, Container, Row } from 'react-bootstrap';
import {
  ProfilePostOne,
  SocialFeedAd,
  SocialFeedFollow,
  SocialFeedForm,
  SocialFeedPostOne,
  SocialFeedPostTwo,
  SocialFeedTrendingTopics,
} from '../widgets';

export default function SocialFeed() {
  return (
    <div className="main-content">
      <Container fluid>
        <Row className="mt-4 mt-md-5">
          <Col xs={12} xl={8}>
            <SocialFeedForm />
            <ProfilePostOne />
            <SocialFeedPostOne />
            <SocialFeedPostTwo />
          </Col>
          <Col xs={12} xl={4}>
            <SocialFeedFollow />
            <SocialFeedTrendingTopics />
            <SocialFeedAd />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
