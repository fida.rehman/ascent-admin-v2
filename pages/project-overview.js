import { Alert, Col, Container, Row } from 'react-bootstrap';
import {
  AnalyticsTotalHours,
  ProjectEarned,
  ProjectHeader,
  ProjectLatestFiles,
  ProjectManagementBudget,
  ProjectManagementCostUnit,
  ProjectManagementProgress,
  ProjectPerformance,
} from '../widgets';

export default function ProjectOverview() {
  return (
    <div className="main-content">
      <ProjectHeader />
      <Container fluid>
        <Row>
          <Col xs={12}>
            <Alert dismissible>This project is forecasted to complete below budget!</Alert>
            <ProjectEarned />
          </Col>
        </Row>
        <Row>
          <Col xs={12} lg={6} xl>
            <ProjectManagementBudget />
          </Col>
          <Col xs={12} lg={6} xl>
            <AnalyticsTotalHours />
          </Col>
          <Col xs={12} lg={6} xl>
            <ProjectManagementProgress />
          </Col>
          <Col xs={12} lg={6} xl>
            <ProjectManagementCostUnit />
          </Col>
        </Row>
        <Row>
          <Col xs={12} xl={8}>
            <ProjectPerformance />
          </Col>
          <Col xs={12} xl={4}>
            <ProjectLatestFiles />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
