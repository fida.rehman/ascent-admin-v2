import { Col, Container, Row } from 'react-bootstrap';
import { Header } from '../components';
import { Dropzone, Quill } from '../components/vendor';
import {
  AnalyticsConversions,
  AnalyticsSales,
  AnalyticsScratchpadChecklist,
  AnalyticsTrafficChannels,
  EcommerceAvgValue,
  EcommerceBestSellingProducts,
  EcommerceConversionRate,
  EcommerceOrdersPlaced,
  EcommerceWeeklySales,
  FeedCTATwo,
  ProfileInfo,
  ProfilePostOne,
  ProjectManagementLatestUploads,
  ProjectPerformance,
  SocialFeedAd,
  SocialFeedTrendingTopics,
  WidgetsActivityList,
  WidgetsActivityTimeline,
  WidgetsContact,
  WidgetsDropzoneMultiple,
  WidgetsKanbanAddCardClose,
  WidgetsKanbanAddCardOpen,
  WidgetsKanbanAddListClose,
  WidgetsKanbanAddListOpen,
  WidgetsKanbanColumn,
  WidgetsKanbanTasks,
  WidgetsNotificationList,
  WidgetsNotificationTimeline,
  WidgetsOrders,
  WidgetsPasswordRequirements,
  WidgetsPricingBasic,
  WidgetsProfileCard,
  WidgetsProfileCardCoverless,
  WidgetsProfileCardLarge,
  WidgetsProfileCardRow,
  WidgetsProfileCardRowLarge,
  WidgetsProfileList,
  WidgetsProjectCard,
  WidgetsProjectCardLarge,
  WidgetsProjectCardRow,
  WidgetsProjectCardRowLarge,
  WidgetsProjectList,
  WidgetsTeamCard,
  WidgetsTeamCardRow,
  WidgetsTeamCardRowLarge,
  WidgetsTeamList,
} from '../widgets';

export default function Invoice() {
  return (
    <div className="main-content">
      <Container fluid>
        <Row className="justify-content-center">
          <Col xs={12} lg={10} xl={8}>
            <Header className="mt-md-5">
              <Header.Body>
                <Row className="align-items-center">
                  <Col>
                    <Header.Pretitle>Overview</Header.Pretitle>
                    <Header.Title>Widgets</Header.Title>
                  </Col>
                </Row>
              </Header.Body>
            </Header>
            <h2 className="mb-3">Projects</h2>
            <p className="text-muted">
              This component family can be used to represent projects, deals, sprints – really any entity that isn't a
              person or company!
            </p>
            <WidgetsProjectCardRowLarge />
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsProjectCardRow />
                <WidgetsProjectCardLarge />
              </Col>
              <Col xs={12} lg={6} className="d-flex flex-column">
                <WidgetsProjectList />
                <WidgetsProjectCard className="card-fill" />
              </Col>
            </Row>
            <hr className="mb-5" />
            <h2 className="mb-3">Users</h2>
            <p className="text-muted">
              Our user primitive is rich enough to easily be used in any type of app, whether it's a social application,
              project management, or customer profiles on an e-commerce site.
            </p>
            <WidgetsProfileCardRowLarge />
            <Row>
              <Col xs={12} lg={6} className="d-flex flex-column">
                <WidgetsProfileCardRow />
                <WidgetsProfileCard />
                <WidgetsProfileCardCoverless className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <WidgetsProfileList />
                <WidgetsProfileCardLarge />
              </Col>
            </Row>
            <hr className="mb-5" />
            <h2 className="mb-3">Teams + Companies</h2>
            <p className="text-muted">
              This collection is meant to represent an organization or group of individuals. It could be a collection of
              users that form a team, or a more abstract representation of a company in your app.
            </p>
            <WidgetsTeamCardRowLarge />
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsTeamCardRow />
                <WidgetsTeamList />
              </Col>
              <Col xs={12} lg={6}>
                <WidgetsTeamCard className="card-fill-lg" />
              </Col>
            </Row>
            <WidgetsTeamCard />
            <hr className="mb-5" />
            <h2 className="mb-3">Graphs + Data</h2>
            <p className="text-muted">
              These data cards are flexible enough to represent all types of data in a uniform fashion. We also enable
              rich animations for multi-layered data...for example, switch the toggle below to see how we animate in new
              data.
            </p>
            <AnalyticsConversions />
            <Row>
              <Col xs={12} lg={6}>
                <AnalyticsTrafficChannels className="card-fill-lg" />
              </Col>
              <Col xs={12} lg={6}>
                <EcommerceWeeklySales />
                <EcommerceOrdersPlaced />
                <EcommerceConversionRate />
                <EcommerceAvgValue />
              </Col>
            </Row>
            <AnalyticsSales />
            <hr className="mb-5" />
            <h2 className="mb-3">Timeline/Activity</h2>
            <p className="text-muted">
              This primitive is meant to make it easy to display both user-centric or activity-centric actions in your
              app. Sometimes it makes sense to even blend both in a single timeline, which works perfectly.
            </p>
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsNotificationTimeline className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <WidgetsActivityTimeline className="card-fill" />
              </Col>
            </Row>
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsNotificationList className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <WidgetsActivityList className="card-fill" />
              </Col>
            </Row>
            <hr className="mb-5" />
            <h2 className="mb-3">Kanban, Tasks, and Checklists</h2>
            <p className="text-muted">
              These can all be used in conjunction for any task-oriented features in your app. We provide all the
              interactions from proper drag-and-drop to rich Kanban style modals for cards.
            </p>
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsKanbanColumn className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <WidgetsKanbanAddListOpen />
                <WidgetsKanbanAddListClose />
                <WidgetsKanbanAddCardOpen />
                <WidgetsKanbanAddCardClose />
              </Col>
            </Row>
            <WidgetsKanbanTasks />
            <AnalyticsScratchpadChecklist />
            <hr className="mb-5" />
            <h2 className="mb-3">Tables, Files, and Lists</h2>
            <p className="text-muted">
              These organizing components show off how you can represent collections of data that needs to be organized,
              filtered, searched, or even uploads for lists of files.
            </p>
            <WidgetsOrders />
            <Row>
              <Col xs={12} lg={6}>
                <ProjectManagementLatestUploads className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <SocialFeedTrendingTopics className="card-fill" />
              </Col>
            </Row>
            <ProjectPerformance />
            <Row>
              <Col xs={12} lg={6}>
                <Dropzone
                  accept="image/*"
                  className="mb-4"
                  multiple={false}
                  onDrop={(acceptedFiles) => console.log(acceptedFiles)}
                />
              </Col>
              <Col xs={12} lg={6}>
                <ProfileInfo />
              </Col>
            </Row>
            <WidgetsDropzoneMultiple />
            <EcommerceBestSellingProducts />
            <hr className="mb-5" />
            <h2 className="mb-3">And so much more...</h2>
            <p className="text-muted">
              We try to think of all possible needs for a wide variety of applications, so we also provide miscellaneous
              components for a host of needs.
            </p>
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsPricingBasic className="card-fill" />
              </Col>
              <Col xs={12} lg={6}>
                <SocialFeedAd />
                <WidgetsPasswordRequirements />
              </Col>
            </Row>
            <FeedCTATwo />
            <Row>
              <Col xs={12} lg={6}>
                <WidgetsContact />
              </Col>
              <Col xs={12} lg={6}>
                <Quill />
              </Col>
            </Row>
            <ProfilePostOne />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
