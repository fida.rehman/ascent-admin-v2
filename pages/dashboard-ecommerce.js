import { Col, Container, Row } from 'react-bootstrap';
import {
  AnalyticsTrafficChannels,
  EcommerceAvgValue,
  EcommerceBestSellingProducts,
  EcommerceConversionRate,
  EcommerceHeader,
  EcommerceOrders,
  EcommerceOrdersPlaced,
  EcommerceRecentActivity,
  EcommerceWeeklySales,
} from '../widgets';

export default function DashboardEcommerce() {
  return (
    <div className="main-content">
      <EcommerceHeader />
      <Container className="mt-n6" fluid>
        <Row>
          <Col xs={12} xl={8}>
            <EcommerceOrders />
          </Col>
          <Col xs={12} xl={4}>
            <AnalyticsTrafficChannels />
          </Col>
        </Row>
        <Row>
          <Col xs={12} lg={6} xl>
            <EcommerceWeeklySales />
          </Col>
          <Col xs={12} lg={6} xl>
            <EcommerceOrdersPlaced />
          </Col>
          <Col xs={12} lg={6} xl>
            <EcommerceConversionRate />
          </Col>
          <Col xs={12} lg={6} xl>
            <EcommerceAvgValue />
          </Col>
        </Row>
        <Row>
          <Col xs={12} xl={4}>
            <EcommerceRecentActivity className="card-adjust-xl" />
          </Col>
          <Col xs={12} xl={8}>
            <EcommerceBestSellingProducts />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
