import FeatherIcon from 'feather-icons-react';
import { Alert, Col, Container, Row } from 'react-bootstrap';
import { AccountApiUsage, AccountCurrentPlan, AccountHeader, AccountInvoices, AccountPaymentMethods } from '../widgets';

export default function AccountBilling() {
  return (
    <div className="main-content">
      <Container fluid>
        <Row className="justify-content-center">
          <Col xs={12} lg={10} xl={8}>
            <AccountHeader />
            <Alert variant="danger">
              <FeatherIcon icon="info" size="1em" className="me-1" /> You are near your API limits.
            </Alert>
            <Row>
              <Col xs={12} md={6}>
                <AccountCurrentPlan />
              </Col>
              <Col xs={12} md={6}>
                <AccountApiUsage />
              </Col>
            </Row>
            <AccountPaymentMethods />
            <AccountInvoices />
            <p className="text-center">
              <small className="text-muted">
                Don’t need Dashkit anymore? <a href="#!">Cancel your account</a>
              </small>
            </p>
            <br />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
