import FeatherIcon from 'feather-icons-react';
import Link from 'next/link';
import { Button, Card, Col, Container, Dropdown, ListGroup, Row } from 'react-bootstrap';
import { Avatar } from '../components';
import { getStatusColor } from '../helpers';
import { TeamHeader } from '../widgets';

export default function ProfilePosts() {
  const data = [
    {
      description: 'You either die Spongebob or you live long enough to...',
      imgSrc: '/img/avatars/profiles/avatar-1.jpg',
      status: 'Online',
      title: 'Dianna Smiley',
    },
    {
      description: 'Working on the latest API integration.',
      imgSrc: '/img/avatars/profiles/avatar-2.jpg',
      status: 'Online',
      title: 'Ab Hadley',
    },
    {
      description: 'Vactioning with the fam',
      imgSrc: '/img/avatars/profiles/avatar-3.jpg',
      status: 'Online',
      title: 'Adolfo Hess',
    },
    {
      description: 'Arts District project management stuff.',
      imgSrc: '/img/avatars/profiles/avatar-4.jpg',
      status: 'Busy',
      title: 'Daniela Dewitt',
    },
    {
      description: 'You either die Spongbob...or become Squidward',
      imgSrc: '/img/avatars/profiles/avatar-5.jpg',
      status: 'Offline',
      title: 'Miyah Myles',
    },
  ];

  const dropdown = (
    <Dropdown align="end">
      <Dropdown.Toggle as="span" className="dropdown-ellipses" role="button">
        <FeatherIcon icon="more-vertical" size="17" />
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Item href="#!">Action</Dropdown.Item>
        <Dropdown.Item href="#!">Another action</Dropdown.Item>
        <Dropdown.Item href="#!">Something else here</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );

  const items = data.map((item, index) => (
    <Card className="mb-3" key={index}>
      <Card.Body>
        <Row className="align-items-center">
          <Col xs="auto">
            <Avatar size="lg">
              <Avatar.Image src={item.imgSrc} className="rounded-circle" alt={item.title} />
            </Avatar>
          </Col>
          <Col className="ms-n2">
            <h4 className="mb-1">
              <Link href="/profile-posts">
                <a>{item.title}</a>
              </Link>
            </h4>
            <Card.Text className="small text-muted mb-1">{item.description}</Card.Text>
            <Card.Text className="small">
              <span className={`text-${getStatusColor(item.status)}`}>●</span> {item.status}
            </Card.Text>
          </Col>
          <Col xs="auto">
            <Button size="sm" className="d-none d-md-inline-block">
              Subscribe
            </Button>
          </Col>
          <Col xs="auto">{dropdown}</Col>
        </Row>
      </Card.Body>
    </Card>
  ));

  return (
    <div className="main-content">
      <TeamHeader />
      <Container fluid>
        <Row>
          <Col xs={12} xl={8}>
            {items}
          </Col>
          <Col xs={12} xl={4}>
            <Card>
              <Card.Body>
                <ListGroup className="list-group-flush my-n3">
                  <ListGroup.Item>
                    <Row className="align-items-center">
                      <Col>
                        <h5 className="mb-0">Member Count</h5>
                      </Col>
                      <Col xs="auto">
                        <small className="text-muted">129</small>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row className="align-items-center">
                      <Col>
                        <h5 className="mb-0">Created</h5>
                      </Col>
                      <Col xs="auto">
                        <time className="small text-muted" dateTime="2018-10-28">
                          10/24/18
                        </time>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row className="align-items-center">
                      <Col>
                        <h5 className="mb-0">Slack Channel</h5>
                      </Col>
                      <Col xs="auto">
                        <a href="#!" className="small">
                          #bloomers
                        </a>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row className="align-items-center">
                      <Col>
                        <h5 className="mb-0">Privacy</h5>
                      </Col>
                      <Col xs="auto">
                        <small className="text-muted">Public</small>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <Row className="align-items-center">
                      <Col>
                        <h5 className="mb-0">Owner</h5>
                      </Col>
                      <Col xs="auto">
                        <a href="#!" className="small">
                          Dianna Smiley
                        </a>
                      </Col>
                    </Row>
                  </ListGroup.Item>
                </ListGroup>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
