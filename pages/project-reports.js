import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { ProjectHeader } from '../widgets';

export default function ProfilePosts() {
  return (
    <div className="main-content">
      <ProjectHeader />
      <Container fluid>
        <Row>
          <Col xs={12}>
            <Card className="card-inactive">
              <Card.Body className="text-center">
                <img className="img-fluid" src="/img/illustrations/scale.svg" style={{ maxWidth: 182 }} alt="..." />
                <h1>No reports yet.</h1>
                <p className="text-muted">Create a report to find our more about this project.</p>
                <Button>Create Report</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
