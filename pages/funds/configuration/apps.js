import { Col, Container, Row, Nav } from 'react-bootstrap';
import { FundConfigurationsHeader } from '../../../widgets';
import React from 'react';
import Accordion from 'react-bootstrap/Accordion';

export default function apps() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={10} xl={8}>
                        <FundConfigurationsHeader />
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header>Singpass/Corppass</Accordion.Header>
                                    <Accordion.Body>
                                        <div class="card-body" style={{display:'flex',flexDirection:'column',justifyContent:'space-evenly'}}>
                                            <div class="card-body" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',alignItems: 'center'}}>
                                                <div>Singpass</div>
                                                <div>Status<select class="form-control mb-3 " style={{paddingRight:'100px'}} name="" id="">
                                                    <option value="">
                                                        Enable
                                                    </option>
                                                    <option value="">
                                                        Disable
                                                    </option>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="card-body" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly'}}>
                                                <div>Corppass</div>
                                                <div>Status<select class="form-control " style={{paddingRight:'100px'}} name="" id="">
                                                    <option value="">
                                                        Enable
                                                    </option>
                                                    <option value="">
                                                        Disable
                                                    </option>
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="1">
                                    <Accordion.Header>Face Verification</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body">
                                            <select class="form-control mb-3 " style={{paddingRight:'100px'}} name="" id="">
                                                <option value="">
                                                    Enable
                                                </option>
                                                <option value="">
                                                    Disable
                                                </option>
                                            </select>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
