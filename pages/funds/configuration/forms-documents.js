import { Col, Container, Row, Nav } from 'react-bootstrap';
import { FundConfigurationsHeader } from '../../../widgets';
import React from 'react';
import Accordion from 'react-bootstrap/Accordion';

export default function formsDocuments() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={10} xl={8}>
                        <FundConfigurationsHeader />
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <form>

                            <div class="row justify-content-between align-items-center">

                                <div class="row">
                                    <div >
                                        <div class="container-fluid">

                                            <input type="radio" value="vibrant" /> All
                                            <input type="radio" value="vibrant" style={{marginLeft:'40px'}} /> Individual
                                            <input type="radio" value="vibrant" style={{marginLeft:'40px'}} /> Corporate
                                            <br />

                                            <div class="card"
                                                data-list='{"valueNames": ["items-type", "items-title", "items-date", "items-kyc", "items-status", "items-client"]}'>
                                                <div class="card-header">


                                                    <form>
                                                        <div class="input-group input-group-flush input-group-merge input-group-reverse">
                                                            <input class="form-control list-search" type="search" placeholder="Search" />
                                                            <span class="input-group-text" >
                                                                <i class="fe fe-search"></i>
                                                            </span>
                                                        </div>
                                                    </form>





                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table table-sm table-nowrap card-table">
                                                        <thead>
                                                            <tr>
                                                                <th>


                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox-all" name="itemsSelect" id="itemsSelectAll"
                                                                            type="checkbox" />
                                                                        <label class="form-check-label" for="itemsSelectAll">&nbsp;</label>
                                                                    </div>

                                                                </th>

                                                                <th>
                                                                    <a href="#" class="text-muted list-sort" data-sort="items-type">
                                                                        Category
                                                                    </a>
                                                                </th>
                                                                <th>
                                                                    <a href="#" class="text-muted list-sort" data-sort="items-title">
                                                                        Customer Type
                                                                    </a>
                                                                </th>

                                                                <th>
                                                                    <a href="#" class="text-muted list-sort" data-sort="items-kyc">
                                                                        Document
                                                                    </a>
                                                                </th>
                                                                <th>
                                                                    <a href="#" class="text-muted list-sort" data-sort="items-kyc">
                                                                        Parent Document
                                                                    </a>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="list">

                                                            <tr>
                                                                <td>


                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox" type="checkbox" name="itemsSelect"
                                                                            id="itemsSelectOne" />
                                                                        <label class="form-check-label" for="itemsSelectOne">&nbsp;</label>
                                                                    </div>

                                                                </td>

                                                                <td class="items-type">
                                                                    Document
                                                                </td>


                                                                <td class="items-kyc">
                                                                    CORPORATE
                                                                </td>
                                                                <td class="items-date">

                                                                    <time datetime="2020-09-30">FORM OF IDENTIFICATION</time>

                                                                </td>
                                                                <td class="items-client">
                                                                    OTHER
                                                                </td>
                                                                <td class="text-end">

                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fe fe-more-vertical"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <a href="#!" class="dropdown-item">
                                                                                Edit
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Export
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox" type="checkbox" name="itemsSelect"
                                                                            id="itemsSelectOne" />
                                                                        <label class="form-check-label" for="itemsSelectOne">&nbsp;</label>
                                                                    </div>

                                                                </td>

                                                                <td class="items-type">
                                                                    Document
                                                                </td>


                                                                <td class="items-kyc">
                                                                    INDIVIDUAL
                                                                </td>
                                                                <td class="items-date">

                                                                    <time datetime="2020-09-30">BANK STATEMENT</time>

                                                                </td>
                                                                <td class="items-client">
                                                                    OTHER
                                                                </td>
                                                                <td class="text-end">

                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fe fe-more-vertical"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <a href="#!" class="dropdown-item">
                                                                                Edit
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Export
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox" type="checkbox" name="itemsSelect"
                                                                            id="itemsSelectOne" />
                                                                        <label class="form-check-label" for="itemsSelectOne">&nbsp;</label>
                                                                    </div>

                                                                </td>

                                                                <td class="items-type">
                                                                    Document
                                                                </td>


                                                                <td class="items-kyc">
                                                                    CORPORATE
                                                                </td>
                                                                <td class="items-date">

                                                                    <time datetime="2020-09-30">TRUST-TRUST DEED / TRUSTEE LETTER</time>

                                                                </td>
                                                                <td class="items-client">
                                                                    OTHER
                                                                </td>

                                                                <td class="text-end">

                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fe fe-more-vertical"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <a href="#!" class="dropdown-item">
                                                                                Edit
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Export
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox" type="checkbox" name="itemsSelect"
                                                                            id="itemsSelectOne" />
                                                                        <label class="form-check-label" for="itemsSelectOne">&nbsp;</label>
                                                                    </div>

                                                                </td>

                                                                <td class="items-type">
                                                                    Document
                                                                </td>


                                                                <td class="items-kyc">
                                                                    INDIVIDUAL
                                                                </td>
                                                                <td class="items-date">

                                                                    <time datetime="2020-09-30">UTILITY/TELEPHONE BILL</time>

                                                                </td>
                                                                <td class="items-client">

                                                                </td>
                                                                <td class="text-end">

                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fe fe-more-vertical"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <a href="#!" class="dropdown-item">
                                                                                Edit
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Export
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <div class="form-check mb-n2">
                                                                        <input class="form-check-input list-checkbox" type="checkbox" name="itemsSelect"
                                                                            id="itemsSelectOne" />
                                                                        <label class="form-check-label" for="itemsSelectOne">&nbsp;</label>
                                                                    </div>

                                                                </td>

                                                                <td class="items-type">
                                                                    Document
                                                                </td>


                                                                <td class="items-kyc">
                                                                    PASSPORT
                                                                </td>
                                                                <td class="items-date">

                                                                    <time datetime="2020-09-30">BANK STATEMENT</time>

                                                                </td>
                                                                <td class="items-client">

                                                                </td>
                                                                <td class="text-end">


                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-bs-toggle="dropdown"
                                                                            aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fe fe-more-vertical"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <a href="#!" class="dropdown-item">
                                                                                Edit
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Export
                                                                            </a>
                                                                            <a href="#!" class="dropdown-item">
                                                                                Delete
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Col>
                </Row>
            </Container>
        </div >
    );
}
