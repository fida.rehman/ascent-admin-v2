import { Col, Container, Row, Nav } from 'react-bootstrap';
import { FundConfigurationsHeader } from '../../../widgets';
import React from 'react';

export default function overview() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={10} xl={8}>
                        <FundConfigurationsHeader />
                    </Col>
                    <Col xs={12} lg={10} xl={8}>

                        <form>

                            <div class="row justify-content-between align-items-center">
                                <div class="col">
                                    <div class="row align-items-center">
                                        <div class="col-auto">

                                            <div class="avatar">
                                                <img class="avatar-img rounded-circle" src="https://media.istockphoto.com/vectors/default-avatar-profile-icon-vector-vector-id1337144146?b=1&k=20&m=1337144146&s=170667a&w=0&h=ys-RUZbXzQ-FQdLstHeWshI4ViJuEhyEa4AzQNQ0rFI=" alt="..." />
                                            </div>

                                        </div>
                                        <div class="col ms-n2">

                                            <h4 class="mb-1">
                                                Upload Fund Logo
                                            </h4>

                                            <small class="text-muted">
                                                PNG or JPG no bigger than 1000px wide and tall.
                                            </small>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">

                                    <button class="btn btn-sm btn-primary">
                                        Upload
                                    </button>

                                </div>
                            </div>
                            <hr class="my-5" />

                            <div class="row">
                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Fund Name
                                        </label>

                                        <input type="text" class="form-control" />

                                    </div>

                                </div>
                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Slug
                                        </label>

                                        <input type="text" class="form-control" />

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Fund Code
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>
                                <div class="col-12 col-md-6">

                                    <div >

                                        <label class="form-label">
                                            Fund Type
                                        </label>
                                        <br />
                                        <select class="form-control mb-3 " name="" id="">
                                            <option value="">
                                                Conventional
                                            </option>
                                            <option value="">
                                                Digital
                                            </option>
                                            <option value="">
                                                Hybrid
                                            </option>
                                        </select>

                                    </div>

                                </div>
                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Region
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Client Name
                                        </label>

                                        <select class="form-control mb-3 " name="" id="">
                                            <option value="">
                                                Client 1
                                            </option>
                                            <option value="">
                                                Client 2
                                            </option>
                                            <option value="">
                                                Client 3
                                            </option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Email
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Phone
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">


                                        <label class="form-label">
                                            Website
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>

                                <div class="col-12 col-md-6">

                                    <div class="form-group">

                                        <label class="form-label">
                                            Support Email
                                        </label>

                                        <input type="text" class="form-control mb-3" />

                                    </div>

                                </div>





                            </div>

                            <button class="btn btn-primary">
                                Save changes
                            </button>

                        </form>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
