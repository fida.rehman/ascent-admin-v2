import { Col, Container, Row, Nav } from 'react-bootstrap';
import { FundConfigurationsHeader } from '../../../widgets';
import React from 'react';
import Accordion from 'react-bootstrap/Accordion';

export default function configuration() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={10} xl={8}>
                        <FundConfigurationsHeader />
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header>Customer Acceptance Form</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body">
                                            <select className="form-control mb-3 " name="" id="">
                                                <option value="">
                                                    Disbale
                                                </option>
                                                <option value="">
                                                    Enable
                                                </option>
                                            </select>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="1">
                                    <Accordion.Header> Additional Redemption</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body">
                                            <select className="form-control mb-3 " name="" id="">
                                                <option value="">
                                                    Disbale
                                                </option>
                                                <option value="">
                                                    Enable
                                                </option>
                                            </select>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="2">
                                    <Accordion.Header>One Going Due Diligence</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body" >
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

                                                <div>Run after every</div>
                                                <div>EXPIRING DOCUMENT DURATION<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <div>Run after every</div>
                                                <div>EXPIRING DOCUMENT DURATION<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <div>Run after every</div>
                                                <div>EXPIRING DOCUMENT DURATION<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="3">
                                    <Accordion.Header>Periodic Review</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body" >
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>

                                                <div>Run after every</div>
                                                <div>DURATION IN MONTHS BETWEEN 1-12<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <div>Run after every</div>
                                                <div>DURATION IN MONTHS BETWEEN 1-12<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <div>Run after every</div>
                                                <div>DURATION IN MONTHS BETWEEN 1-12<input type="text" className="form-control" readonly value="3" /></div>
                                                <div>FREQUENCY
                                                    <select className="form-control mb-3 " name="" id="">
                                                        <option value="">
                                                            Month(s)
                                                        </option>
                                                        <option value="">
                                                            Week(s)
                                                        </option>
                                                        <option value="">
                                                            Day(s)
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                    <Col xs={12} lg={10} xl={8}>
                        <div className="form-group">
                            <Accordion>
                                <Accordion.Item eventKey="4">
                                    <Accordion.Header>Expiring Document</Accordion.Header>
                                    <Accordion.Body>
                                        <div className="card-body" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <div>Run after every</div>
                                            <div>EXPIRING DOCUMENT DURATION<input type="text" className="form-control" readonly value="3" /></div>
                                            <div>FREQUENCY
                                                <select className="form-control mb-3 " name="" id="">
                                                    <option value="">
                                                        Month(s)
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
