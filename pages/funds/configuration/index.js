import { Col, Container, Row, Nav } from 'react-bootstrap';
import { FundConfigurationsHeader } from '../../../widgets';
import React from 'react';

export default function FundConfigurations() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={12} xl={12}>
                        <FundConfigurationsHeader />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
