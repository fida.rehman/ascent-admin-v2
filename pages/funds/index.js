import React, { useMemo } from 'react';
import {
    Col,
    Row,
    Container,
} from 'react-bootstrap';
import {
    DynamicHeader
} from '../../widgets';
import OrganizationFund from './organization/OrganizationFund';
export default function CRMContactsTable({ ...props }) {

    function headerButtonCallBack() {

    }
    return (
        <>
            <div className="main-content">
                <DynamicHeader style={{ marginBottom: '0rem' }} title="Funds" titlesmall="OverView" buttontext={"New Fund"} buttoncallback={headerButtonCallBack} />
                <Container fluid>
                    <Row className="justify-content-center">
                        <Col xs={12}>
                            <OrganizationFund />
                        </Col>
                    </Row>
                </Container>
            </div>


        </>
    );
}
