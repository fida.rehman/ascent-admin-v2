import { Button, Col, Container, Form, Row, Nav } from 'react-bootstrap';
import { ProfileHeader } from '../../widgets';
import React from 'react';
import NumberFormat from 'react-number-format';
import { Avatar } from '../../components';
import { Flatpickr } from '../../components/vendor';
import { AccountHeader } from '../../widgets';
export default function identities() {

    return (
        <div className="main-content">
            <Container fluid>
                <Row className="justify-content-center">
                    <Col xs={12} lg={8} xl={8}>
                        <ProfileHeader />
                        <div class="table-responsive">
                            <table class="table table-sm table-nowrap card-table">
                                <thead>
                                    <tr>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Category
                                        </th>
                                        <th>
                                            Type
                                        </th>
                                        <th>

                                        </th>

                                    </tr>
                                </thead>
                                <tbody class="list">
                                    <tr>
                                        <td>calvin@ascentfs.sg</td>
                                        <td>Entity</td>
                                        <td>Individual</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="dropdown-ellipses dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fe fe-more-vertical"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a href="{{ route('user.profile.identity.individual-summary') }}" class="dropdown-item">
                                                        Detail
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Another action
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Something else here
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>zain@ascentfs.sg</td>
                                        <td>Finance</td>
                                        <td>Bank Account</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="dropdown-ellipses dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fe fe-more-vertical"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a href="{{ route('user.profile.identity.bank-account-summary') }}" class="dropdown-item">
                                                        Detail
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Another action
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Something else here
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>awais@ascentfs.sg</td>
                                        <td>Finance</td>
                                        <td>Digital Wallet</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="dropdown-ellipses dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fe fe-more-vertical"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a href="{{ route('user.profile.identity.digital-wallet-summary') }}" class="dropdown-item">
                                                        Detail
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Another action
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Something else here
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>saad@ascentfs.sg</td>
                                        <td>Entity</td>
                                        <td>Joint</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="dropdown-ellipses dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fe fe-more-vertical"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a href="{{ route('user.profile.identity.corporate-summary') }}" class="dropdown-item">
                                                        Detail
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Another action
                                                    </a>
                                                    <a href="#!" class="dropdown-item">
                                                        Something else here
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <br />
                        <br />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
