import { Col, Container, Row } from 'react-bootstrap';
import { ProfileHeader, ProfileInfo, ProfilePostOne, ProfilePostTwo, ProfileWeeklyHours } from '../widgets';

export default function ProfilePosts() {
  return (
    <div className="main-content">
      <ProfileHeader />
      <Container fluid>
        <Row className="row">
          <Col xs={12} xl={8}>
            <ProfilePostOne />
            <ProfilePostTwo />
          </Col>
          <Col xs={12} xl={4}>
            <ProfileInfo />
            <ProfileWeeklyHours />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
