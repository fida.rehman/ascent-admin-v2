import FeatherIcon from 'feather-icons-react';
import { Card, Col, Container, Form, InputGroup, Row } from 'react-bootstrap';
import { AnalyticsProjects, ProfilePostOne, ProfilePostTwo, TeamHeader, TeamMembers } from '../widgets';

export default function TeamOverview() {
  return (
    <div className="main-content">
      <TeamHeader />
      <Container fluid>
        <Row>
          <Col xs={12} xl={8}>
            <Card>
              <Card.Body>
                <form>
                  <InputGroup className="input-group-merge input-group-flush" size="lg">
                    <Form.Control placeholder="Post to Project" />
                    <InputGroup.Text>
                      <FeatherIcon icon="camera" size="1em" />
                    </InputGroup.Text>
                  </InputGroup>
                </form>
              </Card.Body>
            </Card>
            <ProfilePostOne />
            <ProfilePostTwo />
          </Col>
          <Col xs={12} xl={4}>
            <AnalyticsProjects />
            <TeamMembers />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
