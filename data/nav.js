export default {
  base: {
    children: ['dashboard', 'my_investments', 'entity'],
    id: 'base',
  },
  dashboard: {
    icon: 'grid',
    id: 'dashboard',
    title: 'Dashboard',
    url: '/dashboard',
  },
  funds: {
    icon: 'grid',
    id: 'my_investments',
    title: 'My Investments',
    url: '/subscription/subscription-list',
  },
  clients: {
    icon: 'grid',
    id: 'entity',
    title: 'Entity',
    url: '/user/profile/identity/list',
  },
};
