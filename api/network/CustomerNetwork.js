// export const apiBaseUrl: string = "ht"

import axios from "axios";
import { CancelToken, AxiosResponse } from 'axios';
import { logoutCustomer } from "./CustomerApi";



const TIMEOUT = 60000;


const API = axios.create({
    baseURL: _baseUrl(),
    responseType: 'json',
});

API.interceptors.response.use(function (response) {
    console.log(response,'response')
    if (response?.data?.success == false && response?.data?.status_code?.value == 401) {
        console.log(response,'response in response')
        logoutCustomer();
        // history.push('/signIn');
    }
    return response;
}, function (error) {
    console.log("error", error);
    console.log(error?.toJSON(),'response?.data?.status_code?.value')
    console.log(error?.response?.status,'error?.response?.status')
    if (401 === error?.response?.status) {
        return Promise.reject(error);
    } else {
        return Promise.reject(error);
    }
});


function _baseUrl(){
    console.log(process.env.API_URL);
    return process.env.API_URL
}

export async function processRequest(request, token) {
    const headers = { ...axios.defaults.headers, ...request.headers };
    switch (request.type) {
        case 'GET':
            console.log('API ==>', request.urlString);
            const getResponse = await API.get<R>(request.urlString, { cancelToken: token, headers: headers, timeout: TIMEOUT });
            return getResponse;
        case 'POST':
            const postResponse = await API.post(request.urlString, request.params, { cancelToken: token, headers: headers, timeout: TIMEOUT });
            return postResponse;
        case 'PUT':
            const putResponse = await API.put(request.urlString, request.params, { cancelToken: token, headers: headers, timeout: TIMEOUT });
            return putResponse;
        case 'DELETE':
            const deleteResponse = await API.delete(request.urlString, { cancelToken: token, headers: headers, timeout: TIMEOUT });
            return deleteResponse;
    }
}