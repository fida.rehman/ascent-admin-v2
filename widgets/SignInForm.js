import FeatherIcon from 'feather-icons-react';
import Link from 'next/link';
import React, { useState, useEffect } from "react";
import { Button, Col, Form, InputGroup, Row } from 'react-bootstrap';
import axios, { CancelTokenSource } from "axios";
import {
  loginCustomer,
  setCustomerAuth,
  getFundsDetailAPI,
} from "../api/network/CustomerApi";

export default function SignInForm() {

  const cancelTokenSource = axios.CancelToken.source();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleChangeEmail(e) {
    setEmail(e.target.value)
  }
  function handleChangePassword(e) {
    setPassword(e.target.value)
  }
  const handleSignIn = async () => {
    const response = await loginCustomer(
      email,
      password,
      cancelTokenSource.token
    );
    if (response.success == true) {
     
    }
  };

  return (
    <>
      <h1 className="display-4 text-center mb-3">Sign in</h1>
      {/* <p className="text-muted text-center mb-5">Free access to our dashboard.</p> */}
      <form>
        <div className="form-group">
          <Form.Label>Email Address</Form.Label>
          <Form.Control type="email" placeholder="name@address.com" onChange={(e) => handleChangeEmail(e)} />
        </div>
        <div className="form-group">
          <Row>
            <Col>
              <Form.Label>Password</Form.Label>
            </Col>
            <Col xs="auto">
              <Link href="/password-reset" passHref>
                <Form.Text as="a" className="small text-muted" >
                  Forgot password?
                </Form.Text>
              </Link>
            </Col>
          </Row>
          <InputGroup className="input-group-merge">
            <Form.Control type="password" placeholder="Enter your password" onChange={(e) => handleChangePassword(e)} />
            <InputGroup.Text>
              <FeatherIcon icon="eye" size="1em" />
            </InputGroup.Text>
          </InputGroup>
        </div>
        <Button size="lg" className="w-100 mb-3" onClick={()=>{handleSignIn()}}>
          Sign in
        </Button>
        <p className="text-center">
          <small className="text-muted text-center">
            Don't have an account yet?{' '}
            <Link href="/sign-up">
              <a>Sign up</a>
            </Link>
            .
          </small>
        </p>
      </form>
    </>
  );
}
