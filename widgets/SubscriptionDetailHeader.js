import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Col, Nav, Row } from 'react-bootstrap';
import { Header } from '../components';

export default function AccountHeader({ ...props }) {
  const router = useRouter();

  return (
    <Header className="mt-md-5" {...props}>
      <Header.Body>
        <Row className="align-items-center">
          <Col>
            <Header.Pretitle>INVESTOR PORTAL</Header.Pretitle>
            <Header.Title>Fund Dashboard</Header.Title>
          </Col>
        </Row>
        <Row className="align-items-center">
          <Col>
            <Header.Tabs className="nav-overflow">
              <Nav.Item>
                <Link href="/subscription/detail/overview" passHref>
                  <Nav.Link active={router.pathname === '/subscription/detail/overview'}>Overview</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/subscription/detail/capitalization" passHref>
                  <Nav.Link active={router.pathname === '/subscription/detail/capitalization'}>Capitalization</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/subscription/detail/my-nav" passHref>
                  <Nav.Link active={router.pathname === '/subscription/detail/my-nav'}>My Nav</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/subscription/detail/investments" passHref>
                  <Nav.Link active={router.pathname === '/subscription/detail/investments'}>Investments</Nav.Link>
                </Link>
              </Nav.Item>
            </Header.Tabs>
          </Col>
        </Row>
      </Header.Body>
    </Header>
  );
}
