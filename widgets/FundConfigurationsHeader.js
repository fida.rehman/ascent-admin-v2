import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Col, Nav, Row } from 'react-bootstrap';
import { Header } from '../components';

export default function AccountHeader({ ...props }) {
  const router = useRouter();

  return (
    <Header className="mt-md-5" {...props}>
      <Header.Body>
        <Row className="align-items-center">
          <Col>
            <Header.Pretitle>Fund</Header.Pretitle>
            <Header.Title>Configuration</Header.Title>
          </Col>
        </Row>
        <Row className="align-items-center">
          <Col>
            <Header.Tabs className="nav-overflow">
              <Nav.Item>
                <Link href="/funds/configuration/info" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/info'}>Info</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/funds/configuration/configuration" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/configuration'}>Configuration</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/funds/configuration/apps" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/apps'}>Apps</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/funds/configuration/forms-documents" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/forms-documents'}>Forms & Documents</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/funds/configuration/risk-matrix" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/risk-matrix'}>Risk Matrix</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/funds/configuration/subscriptions" passHref>
                  <Nav.Link active={router.pathname === '/funds/configuration/subscriptions'}>Subscriptions</Nav.Link>
                </Link>
              </Nav.Item>
            </Header.Tabs>
          </Col>
        </Row>
      </Header.Body>
    </Header>
  );
}
