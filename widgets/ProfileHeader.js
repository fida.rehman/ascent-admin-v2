import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Col, Nav, Row, Button, Dropdown } from 'react-bootstrap';
import { Header } from '../components';

export default function AccountHeader({ ...props }) {
  const router = useRouter();

  return (
    <Header className="mt-md-5" {...props}>
      <Header.Body>
        <Row className="align-items-center">
          <Col>
            <Header.Pretitle>OVERVIEW</Header.Pretitle>
            <Header.Title>Profile</Header.Title>
          </Col>
        </Row>
        <Row className="align-items-center">
          <Col>
            <Header.Tabs className="nav-overflow">
              <Nav.Item>
                <Link href="/profile/info" passHref>
                  <Nav.Link active={router.pathname === '/profile/info'}>Info</Nav.Link>
                </Link>
              </Nav.Item>
              <Nav.Item>
                <Link href="/profile/identities" passHref>
                  <Nav.Link active={router.pathname === '/profile/identities'}>Identities</Nav.Link>
                </Link>
              </Nav.Item>

            </Header.Tabs>
          </Col>
          {router.pathname === '/profile/identities' ?
            <Col xs="auto">
              <Dropdown align="end">
                <Dropdown.Toggle as="span" className="dropdown-ellipses" role="button">
                  <Button className="lift">Create New Identity</Button>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="#!">Individual</Dropdown.Item>
                  <Dropdown.Item href="#!">Corporate</Dropdown.Item>
                  <Dropdown.Item href="#!">Digital Wallet</Dropdown.Item>
                  <Dropdown.Item href="#!">Bank Account</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Col>
            : null}
        </Row>
      </Header.Body>
    </Header>
  );
}
